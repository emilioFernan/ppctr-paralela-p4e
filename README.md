# Flame

PPCTR Paralela P4E

## Usage

The student should define the `build` compilation unit (`Makefile`).

Compile

```sh
cd ppctr-paralela-p4e
make build
```

Generic run

```sh
cd ppctr-paralela-p4e
[@1]$ ./build/flame
[@2]$ ./build/flame --client
```

### Compiling Test

I defined some targets. Compile and run the next lines:

```
cd ppctr-paralela-p4e
make build/debug
./build/debug/flame --help
./build/debug/flame --debug
```

Execute the basic `echo` mode (4/12 11:45) using 2 different terminals:

```sh
[@1]$ ./build/debug/flame --debug
[@2]$ ./build/debug/flame --client --debug
```

You should see this:

```
# [@1] server:

Config {
  mode: server
  host: 127.0.0.1
  port: 8000
  preset: Generic
  debug: yes
}
[done] socket
[done] bind
[done] listen
[waiting] accept
[done] accept
[waiting] recv
====recv==== (12B)
op:echo:111
============
[cmd] echo
[echo] parsed value: '111'
[echo] send message: 'echo:112'
[info] client disconnected
[waiting] accept
[done] accept
[waiting] recv
====recv==== (12B)
op:echo:111
============
[cmd] echo
[echo] parsed value: '111'
[echo] send message: 'echo:112'
[info] client disconnected
[exit]


# [@2] client:

Config {
  mode: client
  host: 127.0.0.1
  port: 8000
  preset: Generic
  debug: yes
}
[done] socket
[done] connect
[done] send: 'op:echo:111'
====recv==== (8B)
echo:112
============
[done] socket
[done] connect
[done] send: 'op:echo:111'
====recv==== (8B)
echo:112
============
[exit]
```

### Examples

Echo:

```sh
❯ ./build/debug/flame --debug --protocol Echo
Config {
  mode: server
  host: 127.0.0.1
  port: 8000
  preset: Generic
  protocol: Echo
  debug: yes
}
[done] socket
[done] bind
[done] listen
[waiting] accept
[done] accept
[waiting] recv
====recv==== [12B]
op:echo:111
============
[cmd] echo
[echo] parsed value: '111'
[echo] send message: 'echo:112'
[info] client disconnected
[waiting] accept
[done] accept
[waiting] recv
====recv==== [12B]
op:echo:111
============
[cmd] echo
[echo] parsed value: '111'
[echo] send message: 'echo:112'
[info] client disconnected
[exit]

❯ ./build/debug/flame --client --debug --protocol Echo
Config {
  mode: client
  host: 127.0.0.1
  port: 8000
  preset: Generic
  protocol: Echo
  debug: yes
}
[done] socket
[done] connect
[done] send: 'op:echo:111'
[info] recv timeout
====recv==== [9B]
112
============
[done] disconnect
[done] socket
[done] connect
[done] send: 'op:echo:111'
[info] recv timeout
====recv==== [9B]
112
============
[done] disconnect
[exit]
```

MatMul:

```sh
❯ ./build/debug/flame --debug --protocol MatMul
Config {
  mode: server
  host: 127.0.0.1
  port: 8000
  preset: Generic
  protocol: MatMul
  debug: yes
}
[done] socket
[done] bind
[done] listen
[waiting] accept
[done] accept
[waiting] recv
====recv==== [12B]
op:matmul:3
============
[cmd] matmul
[matmul] parsed dim: '3'
[matmul] filling and computing
A:
 1  1  0
 1  2  1
 1  0  0
B:
 0  1  2
 2  0  0
 0  0  1
C:
 2  1  2
 4  1  3
 0  1  2
[info] client disconnected
[waiting] accept
[done] accept
[waiting] recv
====recv==== [12B]
op:matmul:3
============
[cmd] matmul
[matmul] parsed dim: '3'
[matmul] filling and computing
A:
 1  1  0
 1  2  1
 1  0  0
B:
 0  1  2
 2  0  0
 0  0  1
C:
 2  1  2
 4  1  3
 0  1  2
[info] client disconnected
[exit]

❯ ./build/debug/flame --client --debug --protocol MatMul
Config {
  mode: client
  host: 127.0.0.1
  port: 8000
  preset: Generic
  protocol: MatMul
  debug: yes
}
[done] socket
[done] connect
[done] send: 'op:matmul:3'
====recv==== [44B 2 chunk(s)] matmul: C = A*B:
 2  1  2
 4  1  3
 0  1  2
============
[done] disconnect
[done] socket
[done] connect
[done] send: 'op:matmul:3'
====recv==== [44B 1 chunk(s)] matmul: C = A*B:
 2  1  2
 4  1  3
 0  1  2
============
[done] disconnect
[exit]
```

Stream:

```sh
❯ ./build/debug/flame --debug --protocol Stream
Config {
  mode: server
  host: 127.0.0.1
  port: 8000
  preset: Generic
  protocol: Stream
  debug: yes
}
[done] socket
[done] bind
[done] listen
[waiting] accept
[done] accept
[waiting] recv
====recv==== [84B]
op:stream:
============
[cmd] stream
[stream] chunk: 'El ecologismo (en ocasiones llamado el movimiento verde o ambientalista) ' [73B]
[stream] chunk updated [73B]
[stream] waiting
[stream] chunk: 'es un variado movimiento político, social y global, que defiende la protección del medio ambiente. Recomiendo la película 'Barbacana, la huella del lobo', recientemente proyectada en cines y rodada durante 12 años. Refleja el conflicto por la conservación del lobo ibérico y pone de manifiesto la ineficacia de las medidas actuales.' [338B]
[stream] chunk updated [338B]
[stream] streamed: '4ecd12f1841cd923c04412265691ea6745642864'
[info] client disconnected
[waiting] accept
[done] accept
[waiting] recv
====recv==== [11B]
op:stream:
============
[cmd] stream
[stream] chunk: '' [0B]
[stream] waiting
[stream] chunk: 'El ecologismo (en ocasiones llamado el movimiento verde o ambientalista) ' [73B]
[stream] chunk updated [73B]
[stream] waiting
[stream] chunk: 'es un variado movimiento político, social y global, que defiende la protección del medio ambiente. ' [101B]
[stream] chunk updated [101B]
[stream] waiting
[stream] chunk: 'Recomiendo la película 'Barbacana, la huella del lobo', recientemente proyectada en cines y rodada durante 12 años. Refleja el conflicto por la conservación del lobo ibérico y pone de manifiesto la ineficacia de las medidas actuales.' [237B]
[stream] chunk updated [237B]
[stream] streamed: '4ecd12f1841cd923c04412265691ea6745642864'
[info] client disconnected
[exit]

❯ ./build/debug/flame --client --debug --protocol Stream
Config {
  mode: client
  host: 127.0.0.1
  port: 8000
  preset: Generic
  protocol: Stream
  debug: yes
}
[done] socket
[done] connect
[done] send: 'op:stream:'
[done] send: 'El ecologismo (en ocasiones llamado el movimiento verde o ambientalista) '
[done] send: 'es un variado movimiento político, social y global, que defiende la protección del medio ambiente. '
[done] send: 'Recomiendo la película 'Barbacana, la huella del lobo', recientemente proyectada en cines y rodada durante 12 años. Refleja el conflicto por la conservación del lobo ibérico y pone de manifiesto la ineficacia de las medidas actuales.'
====recv==== [49B 2 chunk(s)] stream: B = fx(A):
4ecd12f1841cd923c04412265691ea6745642864
============
[done] disconnect
[done] socket
[done] connect
[done] send: 'op:stream:'
[done] send: 'El ecologismo (en ocasiones llamado el movimiento verde o ambientalista) '
[done] send: 'es un variado movimiento político, social y global, que defiende la protección del medio ambiente. '
[done] send: 'Recomiendo la película 'Barbacana, la huella del lobo', recientemente proyectada en cines y rodada durante 12 años. Refleja el conflicto por la conservación del lobo ibérico y pone de manifiesto la ineficacia de las medidas actuales.'
====recv==== [49B 2 chunk(s)] stream: B = fx(A):
4ecd12f1841cd923c04412265691ea6745642864
============
[done] disconnect
[exit]
```

## Instructions

I have defined presets (`Preset`) to allow you to change your program at runtime based on the target you are competing with.

Examples:
- you attack (client) the defense (server) of group A, therefore, you can use `flame --client --preset A`.
- you defend (server) the attack (client) of group B, therefore, you can use `flame --preset B`.
- you don't mind about the rival and you don't have special runtime configurations, you use Generic when attacking (`flame --client`) and when defending (`flame`).

I provide operations (`Operation`) to be used when using a protocol:
- Echo.
- MatMul.
- Stream.

I provide protocols (`Protocol`) to operate based on the attack and defense used:
- Echo: both client and server with fast and small messages.
- MatMul: client with fast message but heavy reception, server with slow/complex operations.
- Stream: client with variable length messages and fixed reception, server with complex but updatable messages.
- Mixed: you can mix any type of operation.

### Protocols

This is the real requirement. Trust this specification over the code that I provide.

#### Echo

1. Client connects Server
2. Client sends `op:echo:<number>\0` (null terminated, eg. `op:echo:1300\0`)
3. Server parses the number, increments it, and returns a new message `echo:<number + 1>\0` (null terminated, eg. `echo:1301\0`).
4. Client parses the number and compares it to verify validity (optional).

Note: number should be of type long.

#### MatMul

1. Client connects Server
2. Client sends `op:matmul:<number>\0` (null terminated, eg. `op:echo:10\0`)
3. Server parses the number, creates 3 float matricces with `number` dimension, fills A and B (with `srand(1)` and `srand(2)`, respectively), and multiply both. Then, returns `matmul:\0` (8 Bytes) and the matrix C (floats).
4. Client receives the message, with `matmul:` and the C matrix.

Note: maximum number of dimension is selected randomly at the beginning of every championship (between 0 and 65535, both included).

#### Stream

1. Client connects Server
2. Client sends `op:stream:\0` (11 Bytes, null terminated)
3. Client sends as many messages (not null terminated) as he wants (variable length, type `<char array>`). Client sends the last message ended with `\0` (null terminated). 
4. Server parses the beginning (`op:stream:\0`) and starts processing (OperationStream) all the chunks received until he finds a `\0`. Computes the final value (hex string of type `char array`), and sends it back to the Client, starting with `stream:` (7 Bytes).
5. Client receives the message, with `stream:` and the computed value as string.

## Práctica P4E

Cada grupo tendrá su fichero FLAME.md donde irá escribiendo las ideas y avances conseguidos.

Cada ciclo de trabajo deberá estar compuesto por estos pasos:

1. Brainstorming inicial. Ideas nuevas, redefinición, expectativas. Puede ser una lista de checkbox donde vayas indicando prioridades y el orden de ejecución, ya sea valorando facilidad de implementación o impacto.
2. Desarrolla cada una de las ideas, una a una. Indica que resultados consigues y como lo pruebas (de forma simple). Recuerda que lo más importante es el código y los resultados que consigas.
3. Conclusiones del ciclo de trabajo: tiempo dedicado, malas ideas, dificultades de desarrollo, resultados obtenidos vs esperados, etc.

En todo momento se debe conocer qué hace cada integrante del grupo. A nivel de código contamos con los commits. Las ideas y conclusiones vendrán reflejadas por el autor. Los tres pasos anteriores no tienen por qué realizarse en un solo commit, puedes usar todos los que quieras, pero es improtante que se cuente con los tres puntos de información.

Si en algún momento se hace algo a la vez, como una sesión de pair programming, deberá indicarse.

El Slack es el medio de comunicación principal. Contaremos con deadlines fijos allí notificados. Recuerda que esta práctica va a tener una duracción de aproximadamente 2 semanas, por lo que es importante que te ciñas a los deadlines.

Además, el código cuenta con algunas funcionalidades hechas a propósito "vulnerables", así como sólo dos funciones sin refactorizar (server y client). A medida que los alumnos vayan trabajando iré dando ayudas y notificaciones generales. Por otro lado, existe código que es susceptible de cambiar ante las competiciones, pero os sirve para ir desarrollando, como puede ser el caso de `MAX_REQS` (máximas solicitudes/respuestas antes de finalizar), cuando la competición irá marcada por un tiempo de ejecución (eg. 10 segundos atacando a la defensa).

Absolutamente todo el código que os proporciono es modificable. Os doy una base muy sólida que podéis alterar todo lo que queráis.

Es importante que conozca vuestras ideas de cambio por cada ciclo de trabajo. De esa manera puedo sugeriros por donde tirar, no vaya a ser que se os ocurran cosas muy difíciles de implementar y pueda salvaros de quebraderos de cabeza desde el inicio.

Recuerda que no se tiene solo en cuenta el paralelismo. Aquí cuentan todas la optimizaciones que se nos ocurran, sean del tipo que sean (memoria, algorítmicas, secuenciales, throughput, ataque/pentesting, etc).

Es una práctica en evolución, por lo que puede estar sujeta a pequeñas alteraciones para facilitar la labor del alumno y el disfrute durante su desarrollo. Os iré avisando.

En cualquier momento puedes decidir abandonar la P4E y saltar a la P4, pero recuerda contactar conmigo.

# Author

Original content. PPCTR Professor.

Raúl Nozal <raul.nozal [@] unican.es>