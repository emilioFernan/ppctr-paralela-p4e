#include "matmul.h"

void
matmul_init(char* buffer, unsigned int value)
{
  sprintf(buffer, "op:%s:%d\0", "matmul", value);
}

int
matmul_parse(char* buffer)
{
  unsigned int val;
  int n = sscanf(buffer, "op:matmul:%d", &val);
  return val;
}

unsigned int
matmul_init_response(char* buffer)
{
  sprintf(buffer, "%s:\0", "matmul");
  // padding to 8B
  return strlen(buffer) + 1;
}

float*
matmul_parse_response(char* buffer)
{
  // skip first 8
  if (strncmp(buffer, "matmul:", 7) != 0) {
    return nullptr;
  }
  float* C = (float*)(buffer + 8);
  return C;
}

float**
matmul_alloc(int dim)
{
  float* A = matmul_matrix_alloc(dim);
  float* B = matmul_matrix_alloc(dim);
  float* C = matmul_matrix_alloc(dim);
  float** Ms = (float**)malloc(3 * sizeof(float*));
  Ms[0] = A;
  Ms[1] = B;
  Ms[2] = C;
  return Ms;
}

float*
matmul_matrix_alloc(int dim)
{
  return (float*)malloc(dim * dim * sizeof(float));
}

void
matmul_destroy(float** Ms)
{
  free(Ms[0]);
  free(Ms[1]);
  free(Ms[2]);
  free(Ms);
}

void
matmul_fill(float** Ms, int dim)
{
  float* A = Ms[0];
  float* B = Ms[1];
  srand(1); // time(NULL)
  matmul_matrix_fill(A, dim);
  srand(2);
  matmul_matrix_fill(B, dim);
}

void
matmul_matrix_fill(float* M, int dim)
{
  for (int i = 0; i < dim; i++) {
    for (int j = 0; j < dim; j++) {
      M[i * dim + j] = RAND;
    }
  }
}

void
matmul_matrix_print(float* M, int dim)
{
  for (int i = 0; i < dim; i++) {
    for (int j = 0; j < dim; j++) {
      printf("%2.0f ", M[i * dim + j]);
    }
    printf("\n");
  }
}

void
matmul_compute(float** Ms, int dim)
{
  float* A = Ms[0];
  float* B = Ms[1];
  float* C = Ms[2];
  int i, j, k;

  for (i = 0; i < dim; i++)
    for (j = 0; j < dim; j++)
      C[i * dim + j] = 0.0;

  for (i = 0; i < dim; i++)
    for (j = 0; j < dim; j++)
      for (k = 0; k < dim; k++)
        C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
}
