#include "operations.h"

Operation
match_operation(Operation operation, char* str)
{
  Operation op = operation;
  if (operation == Operation::Echo && strncmp("op:echo:", str, 8) == 0) {
  } else if (operation == Operation::MatMul && strncmp("op:matmul:", str, 10) == 0) {
  } else if (operation == Operation::Stream && strncmp("op:stream:", str, 10) == 0) {
  } else {
    op = Operation::Invalid;
  }
  return op;
}
